<?php

function printbin($val){
$res="";
for ($i=0;$i<32;$i++){
$res.=$val[$i];
if(in_array($i,array(7,15,23))){
$res.=" ";
}
}
return $res;
}

function printbinasip($val){
$res=array();
$s="";
for ($i=0;$i<32;$i++){
$s.=$val[$i];
if(in_array($i,array(7,15,23,31))){
$res[]=binary2int($s);
$s="";
}
}

return join(".",$res);

}




function int2binary($x){
$res="";
$y=strrev(strval($x));
//echo($y);

for ($i=0; pow(2,$i)<=$x;$i++){
//echo $y[$i];
//$x=$x-pow(2,$i+1)."\n";
}

$k=$i;
//echo $k."\n";
//$k=$i;

for ($i=31; $i>=0;$i--){

if($i<=$k){
$d=intdiv($x,pow(2,$i));
//echo $i."->".pow(2,$i).": ".$d."\n";
$x=$x-(pow(2,$i))*$d;
//echo "-$x-\n";
$res.=$d;
}else{
$res.="0";
}

}

return $res;
}



function int2byte($x){
$res="";
$y=strrev(strval($x));
//echo($y);

for ($i=0; pow(2,$i)<=$x;$i++){
//echo $y[$i];
//$x=$x-pow(2,$i+1)."\n";
}

$k=$i;
//echo $k."\n";
//$k=$i;

for ($i=7; $i>=0;$i--){

if($i<=$k-1){
$d=intdiv($x,pow(2,$i));
//echo $i."->".pow(2,$i).": ".$d."\n";
$x=$x-(pow(2,$i))*$d;
//echo "-$x-\n";
$res.=$d;
}else{
$res.="0";
}

}

return $res;
}

function binary($addr){
$chunks=explode('.',$addr);
//var_dump($chunks);

//echo "==>".int2byte(7);

$val="";
foreach($chunks as $v){
$octet=int2byte($v);
//echo $v."==>".$octet."\n";
$val.=$octet;
}
return $val;
}

function binarynetmask($netmask){
$val="";
for($i=0;$i<32;$i++){
if($i>=$netmask){
$val.='0';
}else{
$val.='1';
}
}
return $val;
}

function binarynetmaskaddr($addr,$netmask,$fill){
$val="";
for($i=0;$i<32;$i++){
if($i>=$netmask){
$val.=$fill;
}else{
$val.=$addr[$i];
}
}
return $val;
}

function binary2int($val){
$buf="";
$chk=false;
for($i=0;$i<strlen($val);$i++){
if($val[$i]>0){
$chk=true;
}

if($chk){
$buf.=$val[$i];
}
}

//echo $buf."\n";

$len=strlen($buf)-1;
$val=0;

for($i=0;$i<=$len;$i++){
$val= $val+ (intval($buf[$i]) * pow(2,($len-$i)));
//echo "=".intval($buf[$i])."*".pow(2,($len-$i))."=".intval($buf[$len-$i])*pow(2,($len-$i))."\n";
//break;
}
return $val;

}

