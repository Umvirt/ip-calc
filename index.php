<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Generator" content="SPage (http://umvirt.com/spage)" />
<link href="css/style.css" rel="stylesheet">
<title>UmVirt IP calc</title>
</head>
<body>
<?php
include "ipcalc.php";

$addr="127.0.0.1";
$subnet=24;
$showhosts=false;

if($_REQUEST['addr']){
$addr=$_REQUEST['addr'];
}

if($_REQUEST['subnet']){
$subnet=$_REQUEST['subnet'];
}

if($_REQUEST['showhosts']){
$showhosts=$_REQUEST['showhosts'];
}

$binipval=binary($addr);
$binnetmask=binarynetmask($subnet);
$bin0ip=binarynetmaskaddr($binipval,$subnet,0);
$bin1ip=binarynetmaskaddr($binipval,$subnet,1);

$intzeroip=binary2int($bin0ip);
$intlastip=binary2int($bin1ip);
$hostscnt=$intlastip-$intzeroip-1;


$intfirstusableip=$intzeroip+1;
$intlastusableip=$intlastip-1;
//$intlastusableip=int2binary(255);

$checked="";
echo "<h1>IPv4 calculator</h1>";
echo "<hr>";

echo "<form>";
echo "IPv4-address/Subnet:<br><input name=addr value=$addr>/<input name=subnet size=2 value=$subnet><input type=submit value=\">>\">";
if($showhosts){
$checked="checked";
}
echo "<br><label><input type=checkbox $checked name=showhosts value=yes>Show usable IPv4-addresses (/16 & more)</label>";
echo "</form>";
echo "<hr>";

echo "<b>IPv4-address:</b> $addr<br>";
echo "<b>Subnet:</b> $subnet<br>";
//echo "IP VALUE              : ".printbin($binipval)."<br>";
//echo "NETMASK               : ".printbin($binnetmask)."<br>";
//echo "ZERO IP               : ".printbin($bin0ip)."<br>";
//echo "LAST IP               : ".printbin($bin1ip)."<br>";
///echo "ZERO IP INT           : $intzeroip<br>";
//echo "LAST IP INT           : $intlastip<br>";
echo "<b>Usable hosts count</b>: $hostscnt<br>";
//echo "ZERO IP               : $bin0ip<br>";
//echo "LAST IP               : $bin1ip<br>";
//echo "FIRST USABLE IP (BIN) : ".printbin(int2binary($intfirstusableip))."<br>";
//echo "LAST USABLE IP (BIN)  : ".printbin(int2binary($intlastusableip))."<br>";
echo "<b>First usable host:</b> ".printbinasip(int2binary($intfirstusableip))."<br>";
echo "<b>Last usable host:</b> ".printbinasip(int2binary($intlastusableip))."<br>";
if($hostscnt<=100000 && $showhosts){
echo "<h2>Usable IPv4-addresses</h2>";
$c=0;
for($i=$intfirstusableip;$i<=$intlastusableip; $i++){
$c++;
echo "$c : ".printbinasip(int2binary($i))."<br>";
}
}
?>
</body>
</html>


